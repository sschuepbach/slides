#!/bin/bash

ROOT_DIR=`dirname $( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)`
REVEALJS_DIR="$ROOT_DIR/libs/reveal.js"
CUSTOM_THEMES_DIR="$ROOT_DIR/custom/reveal_themes"

cp $CUSTOM_THEMES_DIR/*.scss $REVEALJS_DIR/css/theme/source/
cd $REVEALJS_DIR
yarn
node_modules/gulp/bin/gulp.js css-themes
