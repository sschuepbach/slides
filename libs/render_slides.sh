#!/bin/bash

ROOT_DIR=`dirname $( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)`
CONTENT_DIR="$ROOT_DIR/content"
SLIDES_DIR="$ROOT_DIR/public/slides"
PANDOC_DIR="$ROOT_DIR/pandoc"
PANDOC_CUSTOM_TEMPLATES_DIR="$ROOT_DIR/custom/pandoc_templates"
PANDOC_TEMPLATES_DIR="$PANDOC_DIR/templates"

if [ -f "$PANDOC_CUSTOM_TEMPLATES_DIR/default.html5" ] && [ -f "$PANDOC_CUSTOM_TEMPLATES_DIR/styles.html" ]; then
    HTML_TEMPLATE_DIR="$PANDOC_CUSTOM_TEMPLATES_DIR/default.html5"
else
    HTML_TEMPLATE_DIR="$PANDOC_TEMPLATES_DIR/default.html5"
fi

if [ -f "$PANDOC_CUSTOM_TEMPLATES_DIR/default.revealjs" ]; then
    REVEALJS_TEMPLATE_DIR="$PANDOC_CUSTOM_TEMPLATES_DIR/default.revealjs"
else
    REVEALJS_TEMPLATE_DIR="$PANDOC_TEMPLATES_DIR/default.revealjs"
fi

if [[ -d "$SLIDES_DIR" ]]
then
    rm -rf "$SLIDES_DIR"
fi
mkdir -p "$SLIDES_DIR"
for s in $CONTENT_DIR/*/index.{md,html}; do
  dirname="$(dirname $s)"
  dirname="${dirname##*/}"
  new_dirname=`echo "$dirname" | tr '[:upper:]' '[:lower:]'`
  mkdir "$SLIDES_DIR/$new_dirname"
  title="${dirname//_/ }"
  basename="${s##*/}"
  if [[ "$basename" == *.md ]]; then
    basename="${basename%%md}html"
    # Create slides, --slide-level 2 is a workaround for the pandoc approach to heading levels
    pandoc -s -t revealjs -o "$SLIDES_DIR/$new_dirname/$basename" -L $PANDOC_DIR/lua-filters/revealjs-codeblock/revealjs-codeblock.lua --slide-level 2 --no-highlight --template $REVEALJS_TEMPLATE_DIR $s
  elif [[ "$basename" == *.html ]]; then
    cp $s "$SLIDES_DIR/$new_dirname"
  fi
  if [ -d "$CONTENT_DIR/$dirname/assets" ]; then
    cp -r "$CONTENT_DIR/$dirname/assets" "$SLIDES_DIR/$new_dirname"
  fi
  echo "* [$title](slides/$new_dirname/$basename)" >> index.md # Create index
done
pandoc --metadata title="Slides Overview" -s -o $ROOT_DIR/public/index.html --template $HTML_TEMPLATE_DIR $ROOT_DIR/index.md && rm $ROOT_DIR/index.md
