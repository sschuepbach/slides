#!/bin/bash

ROOT_DIR=`dirname $( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd)`
CONTENT_DIR="$ROOT_DIR/public/reveal.js"
REVEALJS_DIR="$ROOT_DIR/libs/reveal.js"
HIGHLIGHT_DIR="$ROOT_DIR/custom/highlight_themes"

if [[ -d $CONTENT_DIR ]]; then
    rm -rf $CONTENT_DIR
fi
mkdir -p $CONTENT_DIR
cp -R $REVEALJS_DIR/{dist,plugin} $CONTENT_DIR
if $(ls $HIGHLIGHT_DIR/*.css &>/dev/null); then
    echo "Custom highlight themes detected"
    cp $HIGHLIGHT_DIR/*.css $REVEALJS_DIR/plugin/highlight/
fi
