# My Slides

You can find in this repository some of my slides. See
[`content`](https://gitlab.switch.ch/sschuepbach/slides/-/tree/master/content)
folder for content, https://slides.annotat.io for rendered slides.

## Basic Workflow

The basic workflow, which is triggered in the CI as defined in `.gitlab-ci.yml` or manually by executing the script `local_build.sh`, renders the presentation content in the `content/` folder, compiles it to a reveal.js presentation and saves the result in the `public/` directory (which is not tracked by git). There are some things to consider regarding the [content's layout](#content-management) and the syntax of the [markdown](#markdown) or the html file containing the presentation's text. You can customise the [reveal.js themes](#reveal.js-themes), the 
[code highlight sections](#code-highlight-themes) and the [pandoc templates](#pandoc-templates) as well.

## Content Management

The `content/` directory must have the following layout:

```
.
├── Example_Presentation_1 // => Title of the first presentation
│   ├── index.md // Every subdir needs an index.md or an index.html
│   ├── README.md // Ignored while rendering
│   └── assets // Here go the included files (images, videos etc)
│       └── a_picture.jpg
├── Example_Presentation_2 // => Title of the second presentation
│   ├── index.md // Every subdir needs a index.md or a index.html
│   ├── README.md // Ignored while rendering
│   ├── notes.md // Ignored while rendering
```

The `content/` directory contains a subfolder for each presentation. The name
of the subfolder becomes the title of the presentation as shown in the list on
the homepage. Underscores in the directory name are replaced with whitespaces.
Every presentation directory requires an `index.md` or an `index.html` file,
providing the presentation's content. Additional content (i.e. images and other
media files) goes into the `assets/` subdirectory. All other files in the
presentation directory are ignored in the building process.

## Build process

### CI

The CI process is automatically triggered when a commit in the `main` branch is pushed to a GitLab instance. It's steps are defined in the `gitlab-ci.yml` file.

### Building locally

For a local build execute `./local_build.sh`, which in turn calls some scripts
in the `libs/` directory. The scripts requires the following programs present
on the system:

- bash for running the scripts
- A recent version of [pandoc](https://pandoc.org/) for generating slides from
  markdown files as well as building the index page
- [yarn](https://yarnpkg.com/) (and for that matter
  [nodejs](https://nodejs.org/)) for building custom themes

__Attention!__ All files in the `public/slides` and `public/reveal.js`
directories are deleted when running `./local_build.sh`!

## Markdown

### Front matter

You should define a front matter, e.g. like this:

```
---
revealjs-url: '../../reveal.js'
title: 'Your title here'
subtitle: 'Your subtitle here'
author: 'Your name'
institute: 'Your institute'
date: 2021-12-31
theme: white
highlight: zenburn
css: 'assets/your_css_here.css'
---
```

At the very least you must provide the path to an instance of reveal.js in the
property __`revealjs-url`__. You have two options:

- Use the reveal.js version which is included as submodule in this repository.
  For this purpose, indicate the relative path, i.e. `../../reveal.js`
- Use a reveal.js version from a remote content provider, e.g.
  `https://unpkg.com/reveal.js@latest`.

There are a couple of properties with which you can customise your landing
page. At the very least it is recommended that you set a __`title`__,
otherwise it is set to `index`. Optionally, you can add a __`subtitle`__, too.
Add a __`date`__ if the date of the presentation should be shown on the start
page. It's simply a text, so don't mind about a correct date format. Finally,
you should use the properties __`author`__ and __`institution`__ to generate
show the speaker's name and institute.

The default theme is `black`. Use the __`theme`__ property to override it. There
are a couple of other themes which are bundled with reveal.js. See
[the themes folder](libs/reveal.js/css/theme/source/) for an overview. You can also
create your own theme. See below for some hints.

Choose a highlight theme with the __`highlight`__ property. Make
sure that the respectively named theme is available (see below for
instructions).

The __`css`__ property helps you "tune" the theme. Save the respective file in
the `assets` directory of the content folder in order to make sure that it is
copied to the right destination during the rendering process.

Furthermore, you can use most of the configuration options of reveal.js
itself, e.g. `center` and `transition`. See the
[docs](https://revealjs.com/config/) for a comprehensive list.

Finally, there are some less important properties which you can override in
the front matter. Check out the [`revealjs.template`](reveals.template) for
details.

### Markdown syntax

Generally, you can write markdown syntax with 
[pandoc extensions](https://pandoc.org/MANUAL.html#pandocs-markdown) as basis
for your slides. Consider the following.

#### 2D Navigation

- Use _first-level_ headings (`# Some title`) for starting a new main slide (a
slide on the right side of the previous one)
- Use second-level headings (`## Some sub-title`) for a new sub-slide (a slide
  below the previous one)

#### Slide breaks

Use `---` to start a new slide, e.g.

```markdown
# My title

- On the first page

---

- On the second page
```

While not strictly required, add a blank line before and after the separator to
not confuse the yaml parser.

#### Slide settings

Pandoc's markdown includes the
[header attributes extension](https://pandoc.org/MANUAL.html#extension-header_attributes),
which can be used to define slide-wide settings. These attributes are
translated to HTML as classes, identifiers or arbitrary HTML attributes,
respectively. Example:

```markdown
# My green slide {#my-identifier .my-class data-background-color=green}

Some content
```

As for reveal.js, you can for instance set

* a [background](https://revealjs.com/backgrounds/),
* the [visibility](https://revealjs.com/slide-visibility/) or
* an individual [transition](https://revealjs.com/transitions/).
  
#### Uncover lists incrementally

If you prefix your lists with a closing angle bracket like so `> - my point`,
the list entries become visible incrementally (i.e. when you keep on scrolling
in your slide).

### Customising code blocks

reveal.js offers several options to customise code blocks.

First of all, you have to choose an available highlight.js theme in the front
matter (default is none):

```markdown
---
highlight: monokai
---
```

The most basic annotation of a code block is the syntax highlighting scheme,
e.g.

~~~markdown
```html
<p>
  Some text
</p>
```
~~~

In order to use the [advanced reveal.js code block options](https://revealjs.com/code/),
you must apply the curly braces syntax discussed above. For instance:

~~~markdown
```{.html data-line-numbers="2"}
<p>
  Some text
</p>
```
~~~

#### divs and spans

Using the
[`bracketed_spans`](https://pandoc.org/MANUAL.html#extension-bracketed_spans)
markdown extension, you can define reveal.js attributes on arbitrary text. An
example:

```markdown
[This text will appear later]{.fragment .fade-up data-fragment-index="2"}
```

The same goes for
[`div`s](https://pandoc.org/MANUAL.html#extension-fenced_divs).

#### Speaker notes

Wrap notes (for the speaker's eyes only) in an `<aside>` tag with assigned
`notes` class:

```html
<aside class="notes">
<!-- Your notes here -->
</aside>
```

Notes are per sheet. Text inside `<aside>` blocks are interpreted as markdown,
so you can use e.g. bullet points to make a list.

## Customisation

## reveal.js themes

Use the `custom_themes` folder to define your custom themes. The style
definitions have to be written in Sass (file extension `*.scss`). See
[theme README](libs/reveal.js/css/theme/README.md) for instructions on how to create a theme.

## Code highlight themes

There are two code highlight themes included, `monokai` and `zenburn` (the
former being the default one). If you want to add other themes, you must place
them inside the `highlight_themes` folder. `*.css` files inside this folder
are copied to the right output directory during the publishing step. Have a
look at https://github.com/highlightjs/highlight.js/tree/main/src/styles to
find more themes.

## Pandoc templates

For generating reveal.js presentations and the index.html page, pandoc is used.
It uses templates to tweak the outcome. The default templates reside in the
[`pandoc/templates`](pandoc/templates) folder. You can override the templates
files individually by placing identically named files in the
`custom/pandoc_templates/` directory.
