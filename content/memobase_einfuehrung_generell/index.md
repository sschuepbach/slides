---
title: "Einführung in Memobase"
date: "10. Januar 2023"
revealjs-url: '../../reveal.js'
theme: white
highlight: monokai
css: [assets/main.css]
---

# Inhalt

- Memobase
- Website
- Datenmodell
- Datenmanagement
- Betrieb

# Memobase

## Zweck

- Webportal für das audiovisuelle Kulturerbe der Schweiz
- Zielgruppen: Breite Öffentlichkeit, Lehre, Forschende

## Sammlungen

- [Fotografien](https://memobase.ch/de/object/bbb-002-389533)
- [Tonaufnahmen](https://memobase.ch/de/object/sgv-001-L762B_0505)
- [Radiobeiträge](https://memobase.ch/de/object/srf-020-08d1fba8-3389-486f-98e1-6b487831308d_01)
- [Fernsehbeiträge](https://memobase.ch/de/object/srf-012-A2CC81D4-550F-4761-8A6A-5CD1B053080D_09)
- [Videos](https://memobase.ch/de/object/ikr-001-No_ID_170)
- [Filme](https://memobase.ch/de/object/bar-001-SFW_0013-1)
- [Tonbildschauen](https://memobase.ch/de/object/mfk-002-254913)

## Memoriav

- Betreibt Memobase seit 2001
- Verein zur Erhaltung, Erschliessung und Förderung der breiten Nutzung des audiovisuellen Kulturgutes der Schweiz
- Ist für diese Zwecke Kompetenzstelle und Teil eines grösseren Netzwerks
- Finanziert durch den Bund (Bundesamt für Kultur)

## Netzwerk

Im Moment 111 Institutionen in Memobase verzeichnet

![](./assets/institutionen_klein.png)


## Betrieb

- Projekt "Memobase 2020" seit Anfang 2020:
    - Migrationsphase 2020 - 2021
    - Aktuell Ausbau und Betrieb (bis ca. 2027)
- Rolle UB:
    - "Generalunternehmerin" & Leitung Entwicklung
    - Betrieb Infrastruktur
    - Weiterentwicklung Backend-Komponenten (Importprozesse, Schnittstellen etc.)
- Externe Partnerin für Frontend-Entwicklung zuständig

# Frontend

## Zugänge

- Tiefe Zugangsschwellen durch verschiedene Einstiegspunkte
- Einfache Suche
- Vitrinen
- Kartensuche
- Thematische Suchabfragen (im Fokus)
- Interessante Objekte
- Über Bestände / Institutionen

## Suche

- Einfache Suche (Suchschlitz)
- Fortgeschrittene Suche
- Filter ("Facetten")

## Vitrinen

- Darstellung von kuratierten Inhalten
- Aufträge für Vitrinen z.T. extern 

# Datenmodell

- RDF
- Basiert auf der Records in Context-Ontologie (RiC-O)

## Wesentliche Komponenten

<img height="500px" src="./assets/metadatenmodell_uebersicht.png" />

# Datenmanagement

## Prinzipien

- Microservice-Architektur
    - Services laufen in Containers, Deployment auf Kubernetes
- Kommunikationsflüsse sind _event-driven_
    - Apache Kafka ist Message Broker, dadurch
    - Standardisierte Schnittstelle und
    - dynamische Parallelisierung (in Kombination mit Kubernetes)

## Komponentenübersicht

![](./assets/komponenten.png)

## CMS

CMS: Administratives Backend; es verwaltet...

- Inhalte auf dem Frontend (redaktionelle Texte, Vitrinen)
- Benutzeraccounts
- Bestände und Institutionen
- Importprozesse von Dokumenten

## Importworkflow

![](https://gitlab.com/sschuepbach/diagrams/-/raw/main/public/memobase/import_workflow.png)

## Schnittstellen

- __[RestAPI](https://api.memobase.ch)__: Maschinenlesbare Schnittstelle
- __Medien-Schnittstelle__: Medienauslieferung
    - [Beispiel](https://media.memobase.ch/memo/baz-001-MEI_63730-1/resize/size400x400)
- __IIIF-Schnittstelle__: Zur Einbindung in IIIF-Viewer
    - [Beispiel](https://media.memobase.ch/memo/baz-001-MEI_63730-1/manifest/v2)
- __[OAI-Schnittstelle](https://oai.memobase.ch)__: Export zu Europeana

# Betrieb

## Codemanagement

- Code ist Open-Source
- Coderepository via GitLab von Switch
    - Nutzung der CI-Infrastruktur von GitLab mit dedizierten Runners


## Deployment

- Backend-Workflows basieren auf Microservice-Architektur

# Ressourcen

- [Memobase Frontend](https://memobase.ch)
- [RestAPI](https://api.memobase.ch)
- [Code Repository](https://gitlab.switch.ch/memoriav/memobase-2020)
- [Allgemeine Dokumentation](https://ub-basel.atlassian.net/wiki/spaces/MD/overview)
