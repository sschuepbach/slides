---
title: 'Web Analytics'
revealjs-url: '../../reveal.js'
theme: white
---


# Definition

Web Analytics ist das

- Sammeln,
- Aggregieren und
- Auswerten

der Daten, die Auskunft geben über

das Verhalten von Besucher:innen einer Website

<aside class="notes">
- Erklären: Website ≠ Webseite
</aside>

# Ziele

> - Bessere Kenntnisse über Nutzer:innen einer Website
> - Effektivität von Marketingkampagnen überprüfen
> - Optimierung von Suche, Navigation und Inhalten
> - Konversionsraten (bspw. Besucher:innen zu registrierten Nutzer*innen)
> - Suchmaschinenoptimierung (SEO)
> - A/B Testing

# Workflow

![](https://gitlab.com/sschuepbach/diagrams/-/raw/main/public/digitale_dienste/web_analytics_workflow.png)

# Fragen

> - Popularität einer Website
> - Oft angeschaute Seiten
> - Muster in der zeitlichen Staffelung der Besuche
> - Verhältnis einmalige/wiederkehrende Besucher:innen
> - Wie hoch ist die "Absprungrate"?
> - Wie viele Seiten werden besucht und in welcher Reihenfolge?

## Fragen

> - Durchschnittliche Verweildauer auf Website
> - Häufig genutzte Suchbegriffe
> - Populäre Suchfacetten
> - Verlinkende Seite (sog. Referrer)
> - Wie hoch ist die Registrierungsrate von Besucher*innen?

# Erhebung

> - Analyse des Webserver-Log
> - Setzen von Cookies
> - Zählpixel
> - JavaScript-basierte Verfahren

## Beispiel Log

```
131.152.38.100 - seb [28/Dec/2021:17:01:06 +0100] "GET /docs/Standards/OAI-PMH/ HTTP/2" 200 48778 "https://example.com" "curl/7.80.0"
```

<span class="fragment fade-in" style="font-size:80%">- <span style="color:red">`131.152.38.100`</span>: IP-Adresse des Client</span><br />
<span class="fragment fade-in" style="font-size:80%">- <span style="color:red">`-`</span>: Name des Client, i.d.R. leer</span><br/>
<span class="fragment fade-in" style="font-size:80%">- <span
style="color:red">`seb`</span>: Name des user, vielfach leer</span><br />
<span class="fragment fade-in" style="font-size:80%">- <span style="color:red">`[28/Dec/2021:17:01:06 +0100]`</span>: Zeitstempel des Zugriffs</span><br />
<span class="fragment fade-in" style="font-size:80%">- <span style="color:red">`GET /docs/Standards/OAI-PMH/ HTTP/2`</span>: HTTP-Methode, angeforderte Seite und verwendetes Protokoll</span><br />
<span class="fragment fade-in" style="font-size:80%">- <span
style="color:red">`https://example.com`</span>: Referrer</span><br />
<span class="fragment fade-in" style="font-size:80%">- <span style="color:red">`curl/7.80.0`</span>: User agent</span><br />


# Daten

## Allgemeine Transaktionsdaten

> - Angeforderte Domain (als IP-Adresse)
> - IP-Adresse des Client

## HTTP Request Header Daten

> - User agent: Browsertyp und Browserversion sowie
  verwendetes Betriebssystem
> - Bevorzugte Sprache(n)
> - Referrer (Verlinkende Seite)
> - Cookie (dienen clientseitiger Speicherung von Schlüssel-Wert-Paaren)

<aside class="notes">
Cookie:
- Bringen Zustand in die Zustandslosigkeit von HTTP
- Speichern Schlüssel-Wert-Paare, welche bei jedem weiteren Request vom Client
  auf die Seite mitgegeben wird
- Werden oftmals gebraucht für SessionIDs
</aside>

## JavaScript

> - Hardwarespezifikationen (bspw. CPU-Kerne, Grafikkartenmodell,
  physikalische Kenngrössen der Audiokarte)
> - Browsereinstellungen (bspw. Sichtbarkeit von Menüelementen)
> - Verwendete Browser-Plugins und unterstützte Schriftarten
> - Tastatureingaben und Mausbewegungen

## Fingerprinting

Identifizierung durch eine einzigartige Kombination von Merkmalen, v.a.

- Browsereinstellungen
- Betriebssystemversion
- Hardwareausstattung und -verhalten

Bin ich (bzw. mein Rechner) eindeutig identifizierbar?

=> [My browser fingerprint](https://amiunique.org/fp)

## Verknüpfungen mit Drittquellen

Erhebungen zum Wohnort, Demographie, Interessen etc.

Beispiele:

- DoubleClick
- IP-Lokalisierung

# Tools

> - Externe Tools: Google Analytics / Adobe Analytics
> - On-premise: Matomo
> - Generelle Analysetools: Kibana

# Hürden

> - Eindeutige Identifizierung einer Besucher*in
  * Besucher vs. Sitzungen
  * Ambiguität von IP-Adressen (bspw. Zugriffe über VPN) / Reassignment von
  * (Partielle) Deaktivierung von Trackingmechanismen durch
  Browser-Einstellungen / -Plugins
  IP-Adressen 
> - Relevante Besuche (Crawler)
