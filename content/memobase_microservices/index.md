---
title: 'Microservice-Architektur'
subtitle: 'Am Beispiel Memobase'
date: '17. Januar 2022'
revealjs-url: '../../reveal.js'
theme: beige
highlight: monokai
css: [assets/main.css]
---

# Inhalt

## Die Säulen von Memobase

![](assets/pillars.png)

## Programm

- Weshalb eine Microservice-Architektur für Memobase?
- Was sind Microservices?
- Wie lassen wir Microservices miteinander kommunizieren?
- Wie betreiben wir sie?

<aside class="notes">
- Memobase: Pilotprojekt für Microservice-Architektur
- Inzwischen auch swisscollections
- In Zukunft wird diese bei uns mehr Verwendung finden
</aside>

# Weshalb Microservices?

## Herausforderungen Memobase

- Komplexe Datentransformationsprozesse
- "Entwickeln an den Daten"
- Verschiedene Programmiersprachen / Buildtools, diverse Programmierstile
- Sich verändernde Anforderungen während Entwicklungsphase

<aside class="notes">
- Entwickeln an den Daten:
  - Genaue Datenstruktur und Datenumfang waren zu Beginn des Projektes nicht
    bekannt
  - Einige Überraschungen
- Sich verändernde Anforderungen:
  - Kein herkömmliches Lastenheft
  - Anpassungen, Erweiterungen während Entwicklung
</aside>

## Anforderungen an Softwarearchitektur

- Komponenten unabhängig voneinander erweiterbar
- System durch neue Komponenten leicht ergänzbar
- Vermeiden von Inkompatibilitäten durch unterschiedliche Bibliotheken,
  Programmiersprachen etc.
- Fehlerquellen in Datentransformation rasch lokalisierbar
- Wenige Redundanzen

## Eine (mögliche) Antwort

- Gesamtsystem wird in seine einzelnen Aufgaben aufgedröselt
- Eine Aufgabe <=> eine in sich abgeschlossene Systemkomponente (ein
  __(Micro-)Service__)
- Komponenten kommunizieren über einheitliche Schnittstellen
- Komponenten sind füreinander "Blackboxes"
- Komplexität ist abgekapselt

# Microservices in Memobase {data-background-image="https://media.memobase.k8s.unibas.ch/memo/sag-001-StAGR_FN-IV-40x50-G-011-1/master" data-background-opacity=0.2}

## Übersicht

- Importieren Metadaten zu Objekten, Beständen und Institutionen
- Bereiten Metadaten für Schnittstellen auf
- Analysieren Mediendaten
- Generieren Vorschauelemente
- Löschen Daten
- Erstellen Reports

## Komponenten Importworkflow

![](https://gitlab.com/sschuepbach/diagrams/-/raw/main/public/memobase/import_workflow.png)

## Ein Beispiel: Media-Linker

- Verlinkt Metadatensätze mit Mediendaten
- Mediendaten: Foto, Audiodatei oder Videodatei
- Bei Videodatei ev. zusätzliches Vorschaubild
- Verschiedene Mediendatenquellen

<aside class="notes">
Mediendatenquellen:

- Lokal
- Entfernte Mediendatei
- Youtube
- Vimeo
- SRG
</aside>

# Kafka {data-background-image="https://upload.wikimedia.org/wikipedia/commons/5/57/1961_-_Central_RR_Of_New_Jersey_Roundtable_and_Locomomotive_Yard.jpg" data-background-opacity=0.2}

## Was ist Kafka?

- Plattform für Datenpipelines
- Dient für Services als "Tor" zum Datenaustausch
- Entkoppelt damit kommunizierende Services voneinander (Asynchronität)
- Daher auch Daten(-zwischen-)speicher

## Memobase & Kafka

[Memobase-Importprozess nutzt drei Schnittstellen (APIs) von Kafka]{.tabular}

:::{.fragment .fade-in .tabular .red-block}
___Producer API___: Für Daten"produzenten"

text-file-validator: Liest Datei ein und schreibt Inhalt nach Kafka
:::

:::{.fragment .fade-in .tabular .yellow-block}
___Stream API___: Für Daten"konverter"

media-linker: Liest aus Kafka, reichert Daten mit Medienlink an, und schreibt
Resultat nach Kafka
:::

:::{.fragment .fade-in .tabular .green-block}
___Consumer API___: Für Daten"konsumenten"

media-metadata-indexer: Liest aus Kafka und speichert Datensatz in Datenbank
ab
:::

## Topics: Die "Postfächer" von Kafka

- Daten werden in "Briefen", sog. _Messages_, in Kafka gespeichert
- _Topics_ sind "Postfächer" für Messages
- Reihenfolge von Messages in Topics kann nicht geändert werden

![](https://gitlab.com/sschuepbach/diagrams/-/raw/main/public/memobase/kafka_topics.png)

## Fallbeispiel: Reporting in Memobase

![](https://gitlab.com/sschuepbach/diagrams/-/raw/main/public/memobase/reporting.png)

<aside class="notes">
Beispiel in Kibana zeigen
</aside>


# Kubernetes {data-background-image="https://container-news.com/wp-content/uploads/2019/06/container-ship-white-black-.jpg" data-background-opacity=0.2}

## Virtualisierung

![](https://gitlab.com/sschuepbach/diagrams/-/raw/main/public/memobase/virtualisierung.png)

## Kubernetes

![](https://gitlab.com/sschuepbach/diagrams/-/raw/main/public/memobase/kubernetes_pod_stack.png)

## Vorteile von Kubernetes

- Ressourcenrestriktionen
- Failover bei Hardwareversagen
- Automatische Restarts von Applikationen nach Abstürzen
- (Dynamische) Skalierung von Applikationen
- Namespaces / Tags für unterschiedliche Ausführungsumgebungen
- Darum ideal für Microservice-Architektur!

<aside class="notes">
Microservices

* Werden bei Fehlern neu gestartet
* Hohe Auslastung => grössere Skalierung, mehr Ressourcen
</aside>

## Deklarativer Zugang

1. Gewünschtes Zielsystem wird deklariert (yaml-Datei)
2. Kubernetes kümmert sich um alles weitere

[Einige Beispiele:]{.fragment style="text-align:left"}

>- __Deployments__: Beschreiben eine langanhaltende Applikation
>- __Services__: Machen Pods intern zugreifbar
>- __Ingresses__: Schaffen öffentlichen Zugang zu Service
>- __Jobs__: Einmalige Ausführung einer Applikation

<aside class="notes">
* Beispiel Deployment: media-linker
* Beispiel Service: Datenbank
* Beispiel Ingress: Medienserver
* Beispiel Job: text-file-validator
</aside>

## Memobase & Kubernetes

![](assets/k9s.jpg)
