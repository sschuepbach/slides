---
author: Seb
date: 1. Januar 2022
title: Markdown Example
subtitle: Presenting various Features
revealjs-url: '../../reveal.js'
theme: black
highlight: zenburn
center: false
slideNumber: true
---

# A red slide {data-background-color="red"}

> - This
> - is
> - a
> - fragmented
> - list

## Code example

``` {.sh data-line-numbers="1|3"}
#!/bin/bash

echo "Some text" # This is highlighted in the second place
```

# A (default) black slide

[I'm yet visible]{.fragment .fade-out}
