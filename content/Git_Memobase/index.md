---
title: Memobase<br/>&#128153;<br/>GitLab
date: 11. Januar 2022
revealjs-url: ../../reveal.js
theme: white
css: [assets/code.css]
highlight: zenburn
title-slide-attributes:
    data-background-image: assets/title.jpg
    data-background-opacity: 0.4
---

# Weshalb GitLab?

<table>
<thead>
<tr class="header">
<th>Feature</th>
<th class="memobase-row">Memobase</th>
</tr>
</thead>
<tbody>
<tr class="odd fragment fade-in">
<td>Open Source</td>
<td class="memobase-row"><a href="https://gitlab.switch.ch">gitlab.switch.ch</a></td>
</tr>
<tr class="even fragment fade-in">
<td>Integrierte CI</td>
<td class="memobase-row">Tests, Builds &amp; Packaging</td>
</tr>
<tr class="odd fragment fade-in">
<td>Runners</td>
<td class="memobase-row">4 dedizierte Instanzen</td>
</tr>
<tr class="even fragment fade-in">
<td>Webhooks</td>
<td class="memobase-row">Deployments</td>
</tr>
<tr class="odd fragment fade-in">
<td>Package Registries</td>
<td class="memobase-row">Maven-Registry</td>
</tr>
<tr class="even fragment fade-in">
<td>Container Registries</td>
<td class="memobase-row">Service-Images</td>
</tr>
</tbody>
</table>


# Runners

> - CI-Workflow auf gitlab-Instanz von switch sehr langsam
> - Nutzung von eigenen, dedizierten Runners
> - Laufen als Docker-Containers auf `mb-r1` - `mb-r4`
> - Statisches Set von gecachten Basis-Images
> - Dadurch nur kurze Wartezeiten bei Aufsetzen der Build-Umgebung

# Git-Templates

- Redundanzen vermeiden durch modulare CI-Definition
- Zentrales Repository mit "Templates"

``` {.yaml}
stages:
  - test
  - build
  - publish

include:
  - project: 'memoriav/memobase-2020/utilities/ci-templates'
    file: 'sbt-build/sbt-build.yml'
  - project: 'memoriav/memobase-2020/utilities/ci-templates'
    file: 'docker-image/docker-image.yml'
  - project: 'memoriav/memobase-2020/utilities/ci-templates'
    file: 'helm-chart/helm-chart.yml'
```

# Autodeployment

Ziel: Vereinfachung des Deployment-Workflow auf k8s

![](assets/autodeployment_k8s.png)

## Deployment auf Testumgebung

1. Push von Commits auf `master`-Branch
2. Container image (`latest`-Tag) wird in CI erstellt
3. Webhook ruft Autodeploy-Service auf k8s-Cluster auf
4. Cloning des Repositories durch Autodeploy-Service
5. `helm install` in geklontem Repository

## Deployment auf produktive Umgebung

1. Push von Commits mit semver-kompatiblem Tag, bspw. `1.2.3`
2. Container image (Versions-Tag) wird in CI erstellt
3. Chart wird in CI erstellt und auf lokaler Helm Registry publiziert
4. Webhook ruft Autodeploy-Service auf k8s-Cluster auf
5. Autodeploy-Service installiert Helm Chart direkt von Helm Registry
