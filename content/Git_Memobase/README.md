# Nutzung von git in Memobase

Präsentation zur Nutzung von Git(-lab) für Memobase

## Weiterführende Informationen

### Gitlab allgemein

* [Dokumentation für Nutzer:innen](https://docs.gitlab.com/ee/topics/use_gitlab.html)
* [CI/CD in Gitlab](https://docs.gitlab.com/ee/ci/)
* [`.gitlab-ci.yml Syntaxreferenz](https://docs.gitlab.com/ee/ci/yaml/)
* [Container Image Registries](https://docs.gitlab.com/ee/user/packages/container_registry/)
* [Package Registries](https://docs.gitlab.com/ee/user/packages/package_registry/)

### Gitlab Runner

[Gitlab Runner](https://docs.gitlab.com/runner/) ist eine Applikation, mit
welcher CI-Jobs ausgeführt werden. Eine Runner-Instanz muss nicht zwangsläufig
auf der gleichen Infrastruktur laufen wie die Gitlab-Instanz selbst. Gerade
für ressourcenintensive Build-Jobs lohnt es sich, dedizierte Runners laufen zu
lassen, um nicht von den möglicherweise beschränkten Kapazitäten von den auf
der Gitlab-Instanz zur Verfügungung stehenden _shared runners_ abhängig zu
sein. Dedizierte Runners können für einzelne Gruppen oder sogar einzelne
Projekte
[registriert](https://docs.gitlab.com/ee/ci/runners/runners_scope.html#group-runners)
werden.

Die eigentlichen Jobs werden in sog.
[Executors](https://docs.gitlab.com/runner/executors/) ausgeführt. Gitlab
Runner bietet Executors für eine Reihe von Umgebungen an, allerdings können
viele davon [nicht alle Job-Features
nutzen](https://docs.gitlab.com/runner/executors/#compatibility-chart).
Für Memobase nutzen wir [Docker Executors](https://docs.gitlab.com/runner/executors/docker.html).
