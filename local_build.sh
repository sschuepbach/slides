#!/bin/bash

./libs/render_slides.sh
if $(ls custom/reveal_themes/*.scss &>/dev/null); then
    echo "Custom themes detected"
    ./libs/build_themes.sh
fi
./libs/copy_revealjs.sh
